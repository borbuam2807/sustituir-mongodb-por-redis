import redis

# Establecer la conexión a la base de datos Redis
client = redis.StrictRedis(host='localhost', port=6379, db=0, decode_responses=True)

# Función para agregar una nueva palabra al diccionario
def agregar_palabra():
    palabra = input("Ingrese la palabra en slang panameño: ")
    significado = input("Ingrese el significado de la palabra: ")
    nueva_palabra = {"palabra": palabra, "significado": significado}
    client.hmset(palabra, nueva_palabra)
    print("Palabra agregada exitosamente.")

# Función para editar el significado de una palabra existente
def editar_palabra():
    palabra = input("Ingrese la palabra en slang panameño que desea editar: ")
    palabra_existente = client.hgetall(palabra)
    if palabra_existente:
        nuevo_significado = input("Ingrese el nuevo significado de la palabra: ")
        palabra_existente["significado"] = nuevo_significado
        client.hmset(palabra, palabra_existente)
        print("Palabra editada exitosamente.")
    else:
        print("La palabra no se encontró en el diccionario.")

# Función para eliminar una palabra existente
def eliminar_palabra():
    palabra = input("Ingrese la palabra en slang panameño que desea eliminar: ")
    if client.exists(palabra):
        client.delete(palabra)
        print("Palabra eliminada exitosamente.")
    else:
        print("La palabra no se encontró en el diccionario.")

# Función para mostrar todas las palabras del diccionario
def ver_listado_palabras():
    palabras = client.keys()
    for palabra in palabras:
        palabra_info = client.hgetall(palabra)
        print("Palabra:", palabra_info["palabra"])
        print("Significado:", palabra_info["significado"])
        print("--------------------")

# Función para buscar el significado de una palabra
def buscar_significado():
    palabra = input("Ingrese la palabra en slang panameño que desea buscar: ")
    palabra_info = client.hgetall(palabra)
    if palabra_info:
        print("El significado de la palabra '{}' es: {}".format(palabra, palabra_info["significado"]))
    else:
        print("La palabra no se encontró en el diccionario.")

# Menú principal de la aplicación
while True:
    print("\n*** Diccionario de Slang Panameño ***")
    print("Seleccione una opción:")
    print("a) Agregar nueva palabra")
    print("c) Editar palabra existente")
    print("d) Eliminar palabra existente")
    print("e) Ver listado de palabras")
    print("f) Buscar significado de palabra")
    print("g) Salir")

    opcion = input("Ingrese la opción deseada: ")

    if opcion.lower() == 'a':
        agregar_palabra()
    elif opcion.lower() == 'c':
        editar_palabra()
    elif opcion.lower() == 'd':
        eliminar_palabra()
    elif opcion.lower() == 'e':
        ver_listado_palabras()
    elif opcion.lower() == 'f':
        buscar_significado()
    elif opcion.lower() == 'g':
        print("¡Hasta luego!")
        break
    else:
        print("Opción inválida. Por favor, seleccione una opción válida.")
